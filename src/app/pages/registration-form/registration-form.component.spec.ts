import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { RegistrationFormComponent } from './registration-form.component';

describe('RegistrationFormComponent', () => {
  let component: RegistrationFormComponent;
  let fixture: ComponentFixture<RegistrationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationFormComponent ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate form', () => {
    const compiled = fixture.nativeElement;
    expect(component.registrationForm.invalid).toBeTrue();
    expect(compiled.querySelector('#submit-form').disabled).toBeTrue();
    //now we provide missing values
    component.getControl('dateOfBirth').patchValue('1988-12-08');
    component.getControl('email').patchValue('gaelpat@gmail.com');
    component.getControl('mobilePhone').patchValue('74321');
    fixture.detectChanges();
    expect(component.registrationForm.valid).toBeTrue();
    expect(compiled.querySelector('#submit-form').disabled).toBeFalse();
  });
  it('should submit the form', () => {
    console.log = jasmine.createSpy("log");
    component.getControl('dateOfBirth').patchValue('1988-12-08');
    component.getControl('email').patchValue('gaelpat@gmail.com');
    component.getControl('mobilePhone').patchValue('74321');
    expect(console.log).toHaveBeenCalledWith('1988-12-08');
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    compiled.querySelector('#submit-form').click();
    expect(console.log).toHaveBeenCalledWith('Submitting form', component.registrationForm.value);
  });

  describe('display ui form', () => {

    it('should render courses', () => {
      [
        [0, 'Java'],
        [1, 'Angular'],
        [2, 'C++'],
        [3, 'Kubernetes'],
        [4, 'Spring boot'],
      ].forEach(([index, value]) =>{
        const compiled = fixture.nativeElement;
        expect(compiled.querySelector('#course-label-'+index).textContent).toEqual(value);
      })

      /*const compiled = fixture.nativeElement;
      expect(compiled.querySelector('#course-label-0').textContent).toContain('Java');
      expect(compiled.querySelector('#course-label-1').textContent).toContain('Angular');
      expect(compiled.querySelector('#course-label-2').textContent).toContain('C++');
      expect(compiled.querySelector('#course-label-3').textContent).toContain('Kubernetes');
      expect(compiled.querySelector('#course-label-4').textContent).toContain('Spring boot');*/
    });

    it('should uncheck all courses at start', () => {
      [0, 1, 2, 3, 4].forEach((index) =>{
        const compiled = fixture.nativeElement;
        expect(compiled.querySelector('#course-'+index).checked).toBeFalse();
      })
    });

    it('should prepolutate fields', () => {
      const compiled = fixture.nativeElement;
      expect(compiled.querySelector('#name').value).toEqual('Default');
      expect(compiled.querySelector('#title').value).toEqual('Mrs.');
      expect(compiled.querySelector('#female').checked).toBeTrue();
      expect(compiled.querySelector('#male').checked).toBeFalse();
    });

    it('should not prepolutate fields', () => {
      const compiled = fixture.nativeElement;
      expect(compiled.querySelector('#surname').value).toEqual('');
      expect(compiled.querySelector('#phone').value).toEqual('');
      expect(compiled.querySelector('#date-of-birth').value).toEqual('');
      expect(compiled.querySelector('#email').value).toEqual('');
      expect(compiled.querySelector('#address').value).toEqual('');
    });

    it('should update gender when clicked', () => {
      //check form and ui before
      let compiled = fixture.nativeElement;
      expect(compiled.querySelector('#female').checked).toBeTrue();
      expect(compiled.querySelector('#male').checked).toBeFalse();
      expect(component.registrationForm.value.gender).toEqual('female');
      //click on male radio
      fixture.nativeElement.querySelector('#male').click();
      fixture.detectChanges();
      //check form and ui after
      compiled = fixture.nativeElement;
      expect(compiled.querySelector('#male').checked).toBeTrue();
      expect(compiled.querySelector('#female').checked).toBeFalse();
      expect(component.registrationForm.value.gender).toEqual('male');
    });

  });
});
