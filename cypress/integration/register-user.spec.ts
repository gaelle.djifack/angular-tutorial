describe('Register User', () => {

    beforeEach(() => {
        cy.visit('/registration');
    });

    it('verify initial values', () => {
        cy.get('.card-header').should('have.text', 'Registration Form Title')
        cy.byTestId('name').should('have.value', 'Default')
        cy.byTestId('title').should('have.value', 'Mrs.')
        cy.byTestId('gender-female').should('be.checked')
        cy.byTestId('gender-male').should('not.be.checked')
        cy.byTestId('submit-button').should('be.disabled')
        cy.byTestId('surname').should('be.empty')
        cy.byTestId('address').should('be.empty')
        cy.byTestId('email').should('be.empty')
        cy.byTestId('phone').should('be.empty')
        cy.get('#course-0').should('not.be.checked')
        cy.get('#course-1').should('not.be.checked')
        cy.get('#course-2').should('not.be.checked')
        cy.get('#course-3').should('not.be.checked')
        cy.get('#course-4').should('not.be.checked')
    })

    it('invalidate fields and change css', () => {
        cy.byTestId('name').clear().should('be.empty').and('have.class', 'is-invalid')
        cy.byTestId('date-of-birth').should('be.empty').and('have.class', 'is-invalid')
    })

    it('submit form', () => {
        cy.byTestId('submit-button').should('be.disabled')
        cy.byTestId('title').select('Ms.').should('have.value', 'Ms.')
        cy.byTestId('surname').type('Gaelle')
        cy.byTestId('name').clear().type('Djifack')
        cy.byTestId('date-of-birth').type('1988-12-08')
        cy.byTestId('email').type('gaelle.djifack@gmail.com')
        cy.byTestId('phone').type('17330')
        cy.byTestId('submit-button').should('not.be.disabled')
        cy.byTestId('submit-button').click()
    });
})