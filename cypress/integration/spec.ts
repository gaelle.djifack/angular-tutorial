describe('My First Test', () => {

  beforeEach(() => {
    cy.visit('/');
  });

  it('Visits the initial project page', () => {
    cy.contains('My App')
    cy.contains('Registration form')
  });

  it('Open registration form', () => {
    cy.byTestId('registration-form-link').should('have.text', 'Registration form');
    cy.byTestId('registration-form-link').click();
    cy.get('.card-header').should('have.text', 'Registration Form Title');
  })
})
