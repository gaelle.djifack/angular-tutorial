import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {

  registrationForm: FormGroup;

  coursesList = [
    'Java', 'Angular', 'C++', 'Kubernetes', 'Spring boot'
  ];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.registrationForm = this.fb.group({
      name: ['Default', [Validators.required]],
      surname: [''],
      title: ['Mrs.', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      dateOfBirth: ['', [Validators.required]],
      gender: ['female', [Validators.required]],
      mobilePhone: [null, [Validators.required]],
      address: [''],
      courses: new FormArray([])
    });
    this.initCoursesControls();
    this.addListeners();
  }

  private initCoursesControls(){
    for (let i = 0; i < this.coursesList.length; i++) {
      this.coursesFormGroups.push(
        this.fb.group({
          id: [i],
          checked: [false, [Validators.required]],
          name: [this.coursesList[i]]
        })
      );
    }
  }

  private addListeners(){
    this.registrationForm.get('dateOfBirth').valueChanges
    .subscribe(val => console.log(val));
  }

  /**
   * Getter for the form array
   */
  get coursesFormGroups () {
    return this.getControl('courses') as FormArray
  }

  /**
   * Custom getter for any form control of the formGroup
   * @param name name of the control to get
   * @returns the abstract controls
   */
  getControl(name: string) : AbstractControl{
    return this.registrationForm.get(name);
  }

  submit(){
    console.log('Submitting form', this.registrationForm.value);
  }
}
